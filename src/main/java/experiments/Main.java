package experiments;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class Main
{
    // Zobrazí průběh experimentu jako 100 SVG souborů, šířka 800 px, výška 500 px
    // Každý obrázek obsahuje histogram pro hodnoty 2 až 12 na ose X
    // Využije se toto barevné schéma:
    // #A57878,#A37E77,#A08476,#9E8974,#9B8E73,#999272,
    // #999972,#929972,#8C9972,#859972,#7F9972,#799972

    public static void main(String[] args) throws IOException, ParserConfigurationException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Random rnd = new Random();
        String[] colors = new String[]{
                "#A57878","#A37E77","#A08476","#9E8974","#9B8E73","#999272",
        "#999972","#929972","#8C9972","#859972","#7F9972","#799972"
        };
        int[] counts = new int[13];

        for(int r =0;r<100;r++)
        {
            int random1 = rnd.nextInt(6)+1; // 1 az 6
            int random2= rnd.nextInt(6)+1; // 1 az 6
            int hod = random1+random2;
            counts[hod]++;

            Document document = documentBuilder.newDocument();

            Element svgElement = document.createElement("svg");
            svgElement.setAttribute("width","800");
            svgElement.setAttribute("height","500");
            svgElement.setAttribute("xmlns","http://www.w3.org/2000/svg");
            document.appendChild(svgElement);

            for(int i=2; i<=12; i++)
            {
                // Tady vykreslím sloupec s výškou podle counts[i]
                int width = 40;
                int x = (width + 10) * i;
                int height = counts[i] * 15;
                int y = 500 - height;
                Element rectElement = document.createElement("rect");
                rectElement.setAttribute("width",Integer.toString(width));
                rectElement.setAttribute("height",Integer.toString(height));
                rectElement.setAttribute("x",Integer.toString(x));
                rectElement.setAttribute("y",Integer.toString(y));
                rectElement.setAttribute("fill",colors[i-2]);
                rectElement.setAttribute("stroke","black");
                rectElement.setAttribute("stroke-width","2");

                Element textElement = document.createElement("text");
                textElement.setTextContent(Integer.toString(i));
                textElement.setAttribute("x",Integer.toString(x+20));
                textElement.setAttribute("y","495");
                textElement.setAttribute("text-anchor","middle");
                textElement.setAttribute("font-family","monospace");
                textElement.setAttribute("font-size","30");

                svgElement.appendChild(rectElement);
                svgElement.appendChild(textElement);

            }

            //System.out.println(r);
            System.out.println(hod);
            Path path = Paths.get(System.getProperty("user.dir"), "report","report" + r + ".svg");
            Utils.writeXml(document, new FileOutputStream(path.toString()));
        }

        System.out.println("Hotovo");
    }
}
