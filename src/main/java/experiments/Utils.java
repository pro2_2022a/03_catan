package experiments;

import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Utils
{
    public static void writeXml(Document doc, OutputStream output)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("XS-Loader");
        final LSSerializer writer = impl.createLSSerializer();
        writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
        writer.setNewLine("\n");
        LSOutput lsOutput = impl.createLSOutput();
        lsOutput.setCharacterStream(new OutputStreamWriter(output));
        writer.write(doc, lsOutput);
    }
}
